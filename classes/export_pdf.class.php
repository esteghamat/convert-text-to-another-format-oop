<?php

// include "../vendor/autoload.php";
// use Dompdf\Dompdf;

class export_pdf extends abstract_exporter
{
    public static $ext = 'pdf';
    public function  export()
    {
        $filepath = "upload/{$this->filename}".'.'.$this::$ext;
        
        $dompdf = new Dompdf\DOMPDF();
        $dompdf->loadHtml($this->data);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $output = $dompdf->output();
        file_put_contents($filepath, $output);
        $url = "http://php3.exp:8080/OOP_FormatGenerator/$filepath";
        echo "<a href='$url'>Download Your " . static::$ext . " File</a><br>";

    }
}
