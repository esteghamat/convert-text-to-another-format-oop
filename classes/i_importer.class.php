<?php 

interface i_importer
{
    public function import(string $data, string $filename);
}