<?php

class export_json extends abstract_exporter
{
    public static $ext = 'json';
    public function  export()
    {
        //echo "Txt export run.";
        $filepath = "upload/{$this->filename}".'.'.$this::$ext;
        file_put_contents($filepath, json_encode(array($this->data)));
        $url = "http://php3.exp:8080/OOP_FormatGenerator/$filepath";
        echo "<a href='$url'>Download Your " . static::$ext . " File</a><br>";
    }
}
