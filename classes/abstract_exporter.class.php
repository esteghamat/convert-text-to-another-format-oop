<?php 

abstract class abstract_exporter implements i_exporter,i_importer
{
    protected $data;
    protected $filename;

    public function import(string $data,string $filename)
    {
        $this->data = $data;
        $this->filename = $filename;
    }

    abstract public function  export();
}