<?php
include_once "config.php";
spl_autoload_register("myAutoloader");

function myAutoloader(string $className)
{
    $classFolder = "classes/";
    $classExt = ".class.php";
    $fullPath = $classFolder.$className.$classExt;

    if(file_exists($fullPath))
    {
        //echo "<br>this file included : $fullPath<br>";
        include_once $fullPath;
    }
    else
    {
        echo "<br>this file Not not not Found : $fullPath<br>";
    }
}


