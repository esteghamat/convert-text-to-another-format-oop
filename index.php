<?php
include_once "config.php";
require 'vendor/autoload.php';
use Dompdf\Dompdf;

?>

<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>Flat Responsive Form using CSS3 & HTML5 </title>
  <link rel="stylesheet" href="assets/style.css">

</head>
<body>
<!-- partial:index.partial.html -->
<div id="form-main">
  <div id="form-div">
    <form action="formatGenerator.php" class="form" id="form1" method="POST">
      
      <div class="form-group">
        <p class="filename">
          <input name="filename" type="text" class="validate[required,custom[onlyLetter],length[0,100]] feedback-input" placeholder="FileName" id="filename" />
        </p>
      </div>
      
      <div class="form-group">
        <label for="outputFormat" class="feedbackinputlabel">Choose output format:</label> 
        <select id="outputFormat" name="outputFormat" class="validate[required,custom[onlyLetter],length[0,70]] feedback-input">
          <option value="txt">Text</option>
          <option value="pdf">PDF</option>
          <option value="json">JSON</option>
          <option value="csv">CSV</option>
        </select>
      </div>

      <div class="form-group">
        <p class="text">
          <textarea name="textDataForConvert" class="validate[required,length[6,300]] feedback-input" id="textDataForConvert" placeholder="Text Data"></textarea>
        </p>
      </div>
  
      <div class="submit">
        <input type="submit" value="Generate Format" id="button-blue"/>
        <div class="ease"></div>
      </div>

    </form>
  </div><!--<div id="form-div">-->
</div><!--<div id="form-main">-->
<!-- partial -->

<!--
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
-->

</body>
</html>