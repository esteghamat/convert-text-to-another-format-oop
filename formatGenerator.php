<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

include_once "autoloader.php";
include_once "vendor/autoload.php";
use Dompdf\Dompdf;

$filename = $_POST['filename'];
$ext = $_POST["outputFormat"];
$data = $_POST['textDataForConvert'];

$exportClassName = 'export_'.$ext;

$export_obj = new $exportClassName();
$export_obj->import($data , $filename);
$export_obj->export();


